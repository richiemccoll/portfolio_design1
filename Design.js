if (Meteor.isClient) {
  $(function(){
  var link = $("nav a");
 //click handler
  link.on("click" , function(){
    var $this = $(this);
    var page = $this.data("page");
                  
    $("body").removeClass().addClass(page);
    link.removeClass("active");
    $this.addClass("active");
  })
});
}
if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
