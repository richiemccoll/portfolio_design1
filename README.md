Showing the effectiveness of the simple color change menu with the use of Flexbox and the :after pseudo element!

Design by Richie McColl (https://www.linkedin.com/pub/richie-mccoll/98/ab4/3b0)

The text stays the same throughout but it has a nice effect.

Demo example can be found here (http://tue28072015design.meteor.com)